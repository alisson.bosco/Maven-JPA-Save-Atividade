package alisson;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "clientes" )
public class Cliente {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId( Long id ) {
        this.id = id;
    }

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "cliente_id" )
    private List< Item > itens = new ArrayList<>();

    public void adicionarItem( Item item ) {
        this.itens.add( item );
    }

    public void removerItem( Item item ) {
        this.itens.remove( item );
    }

    public void limparItens() {
        this.itens.clear();
    }

    public List< Item > getItens() {
        return itens;
    }
}