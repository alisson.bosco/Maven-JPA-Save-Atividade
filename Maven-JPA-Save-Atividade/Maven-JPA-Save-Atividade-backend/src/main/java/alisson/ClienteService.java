package alisson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ClienteService {

    private EntityManagerFactory emf;

    public ClienteService() {
        emf = Persistence.createEntityManagerFactory("AlissonDb");
    }

    public void salvarClienteComItens(Cliente cliente, Item[] itens) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            for (Item item : itens) {
                Item itemPersistido = em.find(Item.class, item.getId());
                if (itemPersistido == null || !cliente.getItens().contains(itemPersistido)) {
                    throw new IllegalArgumentException("Item inválido: " + item.getId());
                }
            }
            cliente.limparItens();
            for (Item item : itens) {
                cliente.adicionarItem(item);
            }
            em.merge(cliente);

            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }
}